import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

render(
    <AppContainer>
        <div>
            it works!
        </div>
    </AppContainer>,
    document.getElementById('root')
);
