'use strict';

var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = env => {
    return {
	devtool: 'eval-source-map',
	entry: [
	    'webpack-dev-server/client?http://localhost:8080',
	    'webpack/hot/dev-server',
	    './src/index.js'
	],
	output: {
	    path: path.join(__dirname, '/dist/'),
	    filename: 'bundle.js'
	},
	plugins: [
	    new HtmlWebpackPlugin({
                template: './src/index.tpl.html',
                inject: 'body',
                title: 'My App',
                filename: 'index.html'
	    }),
	    new webpack.DefinePlugin({
                API_HOST: JSON.stringify(env.API_HOST),
                NODE_ENV: JSON.stringify(env.NODE_ENV)
	    }),
	    new webpack.HotModuleReplacementPlugin()
	],
	module: {
	    rules: [
		{
		    test:/\.js$/,
		    exclude: /node_modules/,
		    enforce: 'pre',
		    loader: 'eslint-loader',
		    options: {
			configFile: '.eslintrc',
			failOnWarning: false,
			failOnError: false
		    },
		},
		{
		    test: /\.js?$/,
		    exclude: /node_modules/,
		    loader: 'babel-loader',
		    options: {
			presets: [
			    'react'
			]
		    }
		},
		{
		    test: /\.css$/,
		    loader: 'css-loader'
		}
	    ]
	}
    }
}
